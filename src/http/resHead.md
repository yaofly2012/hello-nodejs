# writeHead、setHeader, headersSent

`head`和`header`区别：
`http`模块的类型方法命名中有`head`和`header`，他们含义不用。
1. `head`表示HTTP报文首部（包含：状态码，状态描述，首部字段）
2. `header`表示指定的首部字段；
3. `headers`表示所有的首部字段。

[req图示](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Overview#%E8%AF%B7%E6%B1%82)
[res图示](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Overview#%E5%93%8D%E5%BA%94)

如果更新`header`并在后续还要获取，则使用`setHeader`更新。

可能是因为这两个方法都可以设置报文首部字段，所以经常拿来对比两者的区别。本质上讲两者没有任何可比性，单独从功能上区分即可。
1. `setHeader`只是单纯的设置报文头部字段；
2. `writeHead`则是发送报文首部数据，并且可以顺便设置首部数据（除了首部字段headers外，还有状态码，状态描述）。

# `headersSent`
## 哪些操作使得`headersSent = true`
1. `res.writeHead`
2. `res.write`
3. `res.end`

方式1是显示地发送报文头部，方式2，3隐式的（隐式header模式）发送报文头。

## `headersSent = true`后？
### 不能：
1. `res.setHeader`
2. `res.writeHead`
3. `res.removeHeader`

即发送headers后，不能再对header进行写的操作（新增，编辑，删除）。

### 能：
1. `res.write`
2. `res.end`

即可以操作报文负载数据（payload）。

# `writableEnded` & `res.end`的行为
`writableEnded`标记`res.end`是否已经执行。
`res.end`必须调用。

## `writableEnded = true`之后
不能再写入数据了，否则报错`ERR_STREAM_WRITE_AFTER_END`。还可以继续调用`res.end`但是都是[无效的](https://github.com/nodejs/node/blob/v12.x/lib/_http_outgoing.js#L747)，但是最好不要这样做。

## `writableEnded`和`writableFinished`区别？？？
`res.writableEnded`表示是否已经调用了`res.end`方法。
`res.writableFinished`表示数据是否已经发送完毕。

## 响应报文处理流程
1. `res.setHeader`
2. `res.writeHead`
3. `res.write`
4. `res.end`
5. ...

# 隐式header模式（implicit header mode）？
即没有显示的调用`writeHead`设置报文首部。Nodejs会有默认的生成报文首部的逻辑。
1. 状态码取`res.statusCode`属性（默认200）
2. 状态描述取`res.statusMessage`属性，如果`res.statusMessage`没有值则取`STATUS_CODES`预定义的。
3. 构建首部字段
具体见[`ServerResponse.prototype._implicitHeader`](https://github.com/nodejs/node/blob/bfe89c0cd72feea7eb05d6b480587f390352e382/lib/_http_server.js#L238)实现（即内部也是调用`writeHead`）：
```js
ServerResponse.prototype._implicitHeader = function _implicitHeader() {
  this.writeHead(this.statusCode);
};
```

# 参考
1. [Node.js response.setHeader() Method](https://www.geeksforgeeks.org/node-js-response-setheader-method/)
2. [Node.js v12.22.7 Documentation](https://nodejs.org/docs/latest-v12.x/api/http.html)
3. [Node.js v12.4.x `_http_server.js`](https://github.com/nodejs/node/blob/bfe89c0cd72feea7eb05d6b480587f390352e382/lib/_http_server.js)
4. [How to use stream.pipe](https://nodejs.org/en/knowledge/advanced/streams/how-to-use-stream-pipe/)