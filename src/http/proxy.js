const http = require('http');
const httpProxy = require('http-proxy');

// // proxy 1
// httpProxy.createProxyServer({
//   target:'http://localhost:8001',
//   xfwd: true,
//   hostRewrite: true,
// })
// .on('proxyReq', (err, req) => {
//   console.group('proxy1');
//   console.log(req.headers);
//   console.log(`req.connection.remoteAddress=${req.connection.remoteAddress}`);
//   console.log(`req.socket.remoteAddress=${req.socket.remoteAddress}`);
//   console.groupEnd('proxy1')
// })
// .listen(8000);

// // proxy 2
// httpProxy.createProxyServer({
//   target:'http://localhost:9000',
//   xfwd: true,
//   hostRewrite: true,
// })
// .on('proxyReq', (err, req) => {
//   console.group('proxy2');
//   console.log(req.headers);
//   console.log(`req.connection.remoteAddress=${req.connection.remoteAddress}`);
//   console.log(`req.socket.remoteAddress=${req.socket.remoteAddress}`);
//   console.groupEnd('proxy2')
// })
// .listen(8001);

// // server
// http.createServer(function (req, res) {
//   console.log(req.headers);
//   console.log(`req.connection.remoteAddress=${req.connection.remoteAddress}`);
//   console.log(`req.socket.remoteAddress=${req.socket.remoteAddress}`);

//   res.writeHead(200, { 'Content-Type': 'text/plain' });
//   res.write('request successfully proxied!' + '\n' + JSON.stringify(req.headers, true, 2));
//   res.end();
// }).listen(9000);


/**
 *  --------------- nodejs ----------- 
 */

// proxy 1
createProxy('127.0.0.1', 3001, '127.0.0.2', 3002);
createProxy('127.0.0.2', 3002, '127.0.0.3', 3003);
createProxy('127.0.0.3', 3003);

function createProxy(host, port, targetHost, targetPort) {
  http.createServer(onRequest).listen(port, host, () => {
    console.log(`App listening on ${host}:${port}`)
  });

  function onRequest(client_req, client_res) {
    console.log('serve: ' + client_req.url);
    if(!targetHost) {
      client_res.write(JSON.stringify(
        {
          msg: `from ${host}:` 
        }
      ));
      client_res.end('ok')
      return;
    }
    //
    console.group(host);
    console.log(client_req.headers);
    console.log(`req.connection.remoteAddress=${client_req.connection.remoteAddress}`);
    console.log(`req.socket.remoteAddress=${client_req.socket.remoteAddress}`);
    console.groupEnd(host);

    const options = {
      hostname: targetHost,
      port: targetPort,
      path: client_req.url,
      method: client_req.method,
      headers: client_req.headers
    };
    const proxy = http.request(options, function (res) {
      client_res.writeHead(res.statusCode, res.headers)
      res.pipe(client_res, {
        end: true
      });
    });
  
    client_req.pipe(proxy, {
      end: true
    });
  }
}
