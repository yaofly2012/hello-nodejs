const http = require('http');

const server = http.Server((req, res) => {
  res.end('<h1>Hello server.listen()</h1>')
});

// 开始监听端口连接
server.listen(3000, () => {
  console.log(`App is runing with port ${server.address().port}`)
})

server.on('listening', () => {
  console.log(`listening`);
});

server.on('connection', socket => {
  console.log(`connection`, socket);
})