const http = require('http');
const port = process.env.PORT || 3000;

const httpServer = http.createServer((req, res) => {
  // res.setHeader('x-name', 'yao1')
  // // Setting up Headers
  // res.setHeader('Content-Type', 'text/html');
  // res.setHeader('Set-Cookie', ['type=ninja', 'language=javascript']);
  
  // // Checking and  printing the headers
  // console.log("When Header is set a string:", 
  // res.getHeader('Content-Type'));
  // console.log("When Header is set an Array:", 
  // res.getHeader('Set-Cookie'));
    
  // Getting the set Headers
  const headers = res.getHeaders();
    
  // Printing those headers
  console.log(headers);
  
  // res.write('data 1')
  // res.write('data 2')
  // Prints Output on the browser in response
  // res.writeHead(
  //   200, 
  //   { 
  //     'Content-Type': 'text/plain',
  //     'x-age': 22
  //   }
  // );

    res.write('data1')
    res.write('data2')
    res.write('data3')
  // Printing those headers
  console.log(res.getHeader('x-age'))
  /**
   * throw new ERR_HTTP_HEADERS_SENT('set')
   * Error [ERR_HTTP_HEADERS_SENT]: Cannot set headers after they are sent to the client
   */
  // res.setHeader('x-name', 'yao3')
  res.end('ok')
  console.log(`res.writableEnded=${res.writableEnded}`)
  console.log(`res.writableFinished=${res.writableFinished}`)
  res.on('finish', () => {
    console.log(`onfinished`)
    console.log(`res.writableEnded=${res.writableEnded}`)
    console.log(`res.writableFinished=${res.writableFinished}`)
  })
  // ERR_STREAM_WRITE_AFTER_END
  // res.write('data 2')
})

httpServer.listen(port, () => {
  console.log(`Server is running at port ${port}...`);
})