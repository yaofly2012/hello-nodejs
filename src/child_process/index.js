const http = require('http');
const {fork} = require('child_process');
const path = require('path');

http.createServer((req, res) => {
    if (req.url === '/sum') {
        //res.end('sum') 
        let childProcess = fork('calc.js', {
            cwd: path.resolve(__dirname)
        });

        childProcess.on('message', function (data) {
            res.end(`${new Date()}`);
        })

    } else {
        res.end('end');
    }
}).listen(8888)