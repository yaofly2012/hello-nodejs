var TEN_SECONDS = ( 10 * 1000 );

function runWithTimeout( command, timeout = TEN_SECONDS ) {

	// Since we can't cancel a command once it has been executed, we have to
	// conditionally turn a command into either a resolved or a rejected promise. In
	// this case, we'll use a timer to generate a rejected promise prior to command
	// completion if the command takes too long to return.
	var promise = new Promise(
		function( resolve, reject ) {

			// Setup our rejection timer to preemptively kill hanging commands.
			var timer = setTimeout(
				function rejectHangingCommand() {

					reject( new Error( "Command took too long (and was timed-out)." ) );

				},
				timeout
			);

			// ************************************************************** //
			// ************************************************************** //
			// CAUTION: This is the line that seemingly makes no sense (to me).
			// I cannot figure out what purpose it actually serves.
			timer.unref();
			// ************************************************************** //
			// ************************************************************** //

			// Catch any synchronous execution errors.
			try {

				var commandPromise = Promise.resolve( command() );

			} catch ( error ) {

				var commandPromise = Promise.reject( error );
			}

			// Once the command returns, we need to use the result to fulfill the
			// contextual Promise. However, the command may return before OR after the
			// timeout has already rejected the contextual Promise. As such, the
			// following operations may actually be No-Op instructions.
			commandPromise.then(
				function handleResolve( result ) {

					clearTimeout( timer );
					resolve( result );

				},
				function handleReject( error ) {

					clearTimeout( timer );
					reject( error );

				}
			);

		}
	);

	return( promise );
}

var promise = runWithTimeout(function command() {
    return new Promise(resolve => {
        setTimeout(() => {
            console.log('API Done')
            resolve('Ok, Response 200')
        }, 5000)
    })
}, 1000)

promise.then(
	function handleResolve( result ) {
		console.log( "Success:", result );
	},
	function handleReject( error ) {
		console.log( "Error:", error );
	}
);