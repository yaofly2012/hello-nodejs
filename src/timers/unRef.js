let count = 0;
const timer = setInterval(() => {
    ++count;
    console.log(`count: ${count}`)
    if(count > 3) {
        clearInterval(timer)
    }
}, 1000).unref();


// setImmediate(() => {
//     console.log(`setImmedicate`)
// }).unref()