async function async1() {
    await async2();
    console.log('async1 end');
  }
  
  async function async2() {
    return new Promise(resolve => {
      console.log('async2 promise');
      resolve(32); 
    })
  }
  
  async1();
  
  new Promise(resolve => {
    resolve();
  }).then(() => {
    console.log('promise2');
  })