const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;

const maxWorkerCount = Math.min(2, numCPUs);

if(cluster.isMaster) {
  logger(`CPU number is ${numCPUs}`, cluster.settings.exec) // cluster.settings.exec undefined ?
  logger(`Master is running`);

  let numReqs = 0;
  setInterval(() => {
    logger(`numReqs = ${numReqs}`);
  }, 1000);

  cluster.on('listening', (worker, address) => {
    logger(
      `A worker ${worker.process.pid} is now connected to ${address.address}:${address.port}`
    );
  });

  cluster.on('exit', (code, signal) => {
    if (signal) {
      logger(`worker was killed by signal: ${signal}`);
    } else if (code !== 0) {
      logger(`worker exited with error code: ${code}`);
    } else {
      logger('worker exited!');
    }
  });

  // fork
  for(let i = 0; i < maxWorkerCount; ++i) {
    logger(`Fork worker ${i + 1}`);
    const worker = cluster.fork({
      index: i
    }); // 分叉

    worker.on('message', msg => {
      if (msg.cmd && msg.cmd === 'notifyRequest') {
        numReqs += 1;
      }
    });

    worker.on('disconnect', () => {
      logger(`Worker ${worker.process.pid} disconnect`)
    });
  }
} else {
  logger(`Worker-${process.env.index} is running`, cluster.worker.process === process);

  const server = http.createServer((req, res) => {
    res.writeHead(200);
    res.end(`${process.pid}: Hello World\n`);
    logger(`Handle request ${req.url}`);
    process.send({ cmd: 'notifyRequest' });  // Tell master
  });

  server.listen(8000, (err) => {
    logger(`App start ${!err ? 'successed' : 'falied'}`)
  });

  server.on('connection', socket => {
    logger('connection handler', socket.server._connections);
  });
}

function logger(...args) {
  return console.log(
    `[${cluster.isPrimary ? 'Master' : 'Worker'} ${process.pid}] `, 
    ...args
  );
}