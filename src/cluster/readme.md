JS文件会被多次执行，每个进程都会各自加载执行一次。

Cluster是进程集群，也叫进程池（worker pool）。

# 基本能力
1. 创建worker进程
2. IPC通信
- master与worker
- worker与worker 只能通过master进程间接通信

>worker communicate with the parent via IPC and pass server handles back and forth

# 高阶能力
1. 共享端口
2. worker调度

## 调度策略
1. 轮询（默认，除了windows）
2. 主进程创建socket，将socket传给worker，worker直接处理请求？？？
依赖操作系统调度？

Node.js does not provide routing logic.？
难道调度策略不是路由么？

## Worker进程和一般进程`server.listen()`差异

# API
`cluster`和`worker`的事件名称很多一样的，并且有些事件来自`net.Server`(listening)
事件名称|cluster|worer|备注|
|--|--:|--:|--|
|`listening`|Y|Y|源自`net.Server`|
|`online`|Y|Y|
|`message`|Y|Y|
|`disconnect`|Y|Y|
|`exit`|Y|Y|
|`setup`|Y|N|
|`error`|N|Y|源自`child_process.fork()`|


## Worker
1. dead ? 表示worker是否终止了
2. connected ? 表示worker是否跟master进程建立IPC连接

# 不提供能力
1. Node.js does not provide routing logic. ？
2. 不管理worker池，只是提供相关方法。

# 原理
`一个master进程下HTTP代理服务器 + 多个worker进程多个HTTP应用服务器`架构。

1. master进程里并没有显示的创建HTTP服务，那是如何监听请求的？
2. master进程可以创建多少服务？
3. 客户端是跟主进程还是worker进程建立连接的（即跟谁发生HTTP握手的）？
跟master进程

搞清原理先弄清：
1. `child_process`
2. `net.Server.listen`
3. TCP/IP三次握手，TCP建立连接的信息 IP:port

## [`cluster`模块](https://github.com/nodejs/node/blob/v16.18.1/lib/cluster.js)是如此简单
```js
'use strict';

const childOrPrimary = 'NODE_UNIQUE_ID' in process.env ? 'child' : 'primary';
module.exports = require(`internal/cluster/${childOrPrimary}`);
```

[`Server.prototype.listen`实现](https://github.com/nodejs/node/blob/v16.18.1/lib/net.js#L1544)（认真读读）

worker进程创建了`net.Server`实例`server`，但是调用`server.onconnection`方法方式变化了。
一般进程中`net.Server.prototype.onconnection`调用流程：
1. nodejs在C++底层监听到连接 
2. 调用JS层的`net.Server.prototype.onconnection`函数。

worker进程中`net.Server.prototype.onconnection`调用流程：
1. nodejs在C++底层监听到连接
2. 调用主进程JS层的`net.Server.prototype.onconnection`函数
3. 主进程通过策略选择合适的worker进程，并程触发`newconn`事件（把`socket`句柄传给worker进程）；
4. worker进程响应`newconn`，并调用worker进程里`net.Server`实例的`onconnection`方法。

## 调度策略
非window操作系统中cluster模块默认采用RoundRobin（RR）调度策略。
### 1. RR策略
主进程的`net.Server`实例的`onconnection`被重写。
[round_robin_handle代码片段](https://github.com/nodejs/node/blob/v16.18.1/lib/internal/cluster/round_robin_handle.js#L45)：
```js
this.handle.onconnection = (err, handle) => this.distribute(err, handle);
```
即主进程的`net.Server`实例只负责分发请求，并且透传参数。

### 2. （Shared Socket）SS策略
共享socket策略。子进程创建的TCP服务器会在底层侦听端口并处理响应。
但是如何避免共享端口报错呢？
在C++层设置端口的`SO_REUSEADDR`选项，由操作系统内核来进行调度。

# PK `child_process`
`cluster`模块是对`child_process`模块的一层封装，worker进程就是利用`child_process.fork()`创建的。

# IPC channel

# 参考
1. [node.js之cluster集群初探-1](https://www.cnblogs.com/novak12/p/9298353.html)
2. [node.js之cluster集群初探-2](https://www.cnblogs.com/novak12/p/9304617.html)
3. [Nodejs cluster模块深入探究（值得反复阅读）](https://www.cnblogs.com/accordion/p/7207740.html)
4. [通过Node.js的Cluster模块源码，深入PM2原理](https://cloud.tencent.com/developer/article/1625697)